using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "NewEnemyData", menuName = "GameData/EnemyData", order = 0)]
    public class EnemyData : ScriptableObject
    {
        public int Health;
        public float Speed;
        public int Money;
    }
}