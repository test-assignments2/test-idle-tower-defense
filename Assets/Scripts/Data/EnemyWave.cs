using System;

namespace Data
{
    [Serializable]
    public class EnemyWave
    {
        public int EnemyQuantity;
        public float DelayBetweenSpawnEnemy;
    }
}