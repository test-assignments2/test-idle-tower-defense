using System;

namespace Data
{
    [Serializable]
    public class FortressCharacteristics<T> where T : IComparable
    {
        public T Value;
        public int UpgradePrice;
    }
}