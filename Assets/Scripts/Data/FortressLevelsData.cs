using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "NewFortressLevelsData", menuName = "GameData/FortressLevels", order = 0)]
    public class FortressLevelsData : ScriptableObject
    {
        public List<FortressCharacteristics<int>> Damages;
        public List<FortressCharacteristics<float>> FireRate;
        public List<FortressCharacteristics<float>> FireRange;
    }
}