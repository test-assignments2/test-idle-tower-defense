using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "NewLevelData", menuName = "GameData/GameLevels", order = 0)]

    public class LevelsData : ScriptableObject
    {
        public float DelayBetweenWaves;
        public List<EnemyWave> Waves;
    }
}