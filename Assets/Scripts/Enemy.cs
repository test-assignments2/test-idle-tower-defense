using System;
using UnityEngine;

[RequireComponent(
    typeof(Rigidbody2D),
    typeof(Collider2D))]
public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject _deathEffect;
    
    public event Action<Enemy> OnDeath;
    public int Money { get; private set; }
    public int Health { get; private set; }
    
    private float _speed;
    private Rigidbody2D _rigidbody;
    

    public void Init(Vector2 target, int health, float speed, int money)
    {
        Health = health;
        _speed = speed;
        Money = money;
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();
        Vector2 direction = (target - (Vector2)transform.position).normalized;
        RoteToTarget(direction);
        _rigidbody.velocity = direction * _speed;
    }

    private void RoteToTarget(Vector2 direction)
    {
        Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, direction);
        targetRotation = Quaternion.Euler(new Vector3(0, 0, targetRotation.eulerAngles.z));
        transform.rotation = targetRotation;
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            Instantiate(_deathEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent(out Fortress fortress))
        {
            fortress.TakeDamage();
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        OnDeath?.Invoke(this);
    }
}