using System.Collections.Generic;
using UnityEngine;

[RequireComponent(
    typeof(Rigidbody2D),
    typeof(Collider2D))]
public class EnemyDetector : MonoBehaviour
{
    private List<Enemy> _enemies = new List<Enemy>();

    public bool TryGetClosestEnemy(out Enemy closestEnemy)
    {
        closestEnemy = null;
        if (_enemies.Count > 0)
        {
            float closestDistance = Mathf.Infinity;
            foreach (var enemy in _enemies)
            {
                float distanceToEnemy = Vector2.Distance(transform.position, enemy.transform.position);
                if (distanceToEnemy < closestDistance)
                {
                    closestEnemy = enemy;
                    closestDistance = distanceToEnemy;
                }
            }
        }
        return _enemies.Count > 0;
    }

    public void SetRadius(float radius)
    {
        transform.localScale = new Vector3(radius, radius);
    }

    public void RemoveEnemy(Enemy enemy)
    {
        if (_enemies.Contains(enemy))
        {
            _enemies.Remove(enemy);
            enemy.OnDeath -= RemoveEnemy;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent(out Enemy enemy))
        {
            _enemies.Add(enemy);
            enemy.OnDeath += RemoveEnemy;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out Enemy enemy))
        {
            RemoveEnemy(enemy);
        }
    }
}