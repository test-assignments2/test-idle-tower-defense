using System;
using Data;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private EnemyData _enemyData;
    [SerializeField] private Transform _spawnZoneCenter;
    [SerializeField] private float _spawnRadius = 3;
    [SerializeField] private Enemy _enemyPrefab;

    public Enemy SpawnEnemy()
    {
        float angle = Random.Range(0f, (float)Math.PI * 2f);
        Vector2 spawnPosition = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * _spawnRadius +
                                (Vector2)_spawnZoneCenter.transform.position;

        var enemy = Instantiate(_enemyPrefab, spawnPosition, Quaternion.identity);
        enemy.Init(_spawnZoneCenter.transform.position,_enemyData.Health,_enemyData.Speed,_enemyData.Money);
        return enemy;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.red;
        Handles.DrawWireDisc(_spawnZoneCenter.position, Vector3.back, _spawnRadius);
    }
#endif
}