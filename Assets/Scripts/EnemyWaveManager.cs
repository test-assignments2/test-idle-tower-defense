using System.Collections;
using System.Collections.Generic;
using Data;
using DefaultNamespace;
using UI;
using UnityEngine;

public class EnemyWaveManager : MonoBehaviour
{
    [SerializeField] private EnemySpawner _enemySpawner;
    [SerializeField] private MoneyCounter _moneyCounter;
    [SerializeField] private RootUI _rootUI;
    [SerializeField] private LevelsData _levelsData;

    private List<Enemy> _currentEnemies = new List<Enemy>();
    private int _currentWaveNumber = 0;
    private Coroutine _currentWave;

    public void StartGame()
    {
        _moneyCounter.Reset();
        _currentWaveNumber = 0;
        _currentWave = StartCoroutine(StartWave(_levelsData.Waves[_currentWaveNumber]));
        _rootUI.BottomPanel.SetWave(_currentWaveNumber + 1,_levelsData.Waves[_currentWaveNumber].EnemyQuantity);
    }

    public void GameOver()
    {
        StopCoroutine(_currentWave);
        for (int i = 0; i < _currentEnemies.Count; i++)
        {
            _currentEnemies[i].OnDeath -= RemoveEnemy;
            Destroy(_currentEnemies[i].gameObject);
        }
        _currentEnemies.Clear();
        _rootUI.ShowGameOver(GameOverStatus.Lose);
    }

    private IEnumerator StartWave(EnemyWave wave)
    {
        yield return new WaitForSeconds(_levelsData.DelayBetweenWaves);
        WaitForSeconds delay = new WaitForSeconds(wave.DelayBetweenSpawnEnemy);
        _rootUI.BottomPanel.SetWave(_currentWaveNumber + 1,wave.EnemyQuantity);
        for (int i = 0; i < wave.EnemyQuantity; i++)
        {
            AddEnemy(_enemySpawner.SpawnEnemy());
            yield return delay;
        }
    }

    private void AddEnemy(Enemy enemy)
    {
        _currentEnemies.Add(enemy);
        enemy.OnDeath += RemoveEnemy;
    }

    private void RemoveEnemy(Enemy enemy)
    {
        _moneyCounter.AddMoney(enemy.Money);
        _currentEnemies.Remove(enemy);
        enemy.OnDeath -= RemoveEnemy;
        if (_currentEnemies.Count == 0)
        {
            if (_currentWaveNumber < _levelsData.Waves.Count - 1)
            {
                _currentWaveNumber++;
                _currentWave = StartCoroutine(StartWave(_levelsData.Waves[_currentWaveNumber]));
            }
            else
            {
                _rootUI.ShowGameOver(GameOverStatus.Win);
            }
        }
        _rootUI.BottomPanel.SetWaveValue(_currentEnemies.Count);
    }

    private void OnDestroy()
    {
        foreach (var enemy in _currentEnemies)
        {
            enemy.OnDeath -= RemoveEnemy;
        }
    }
}