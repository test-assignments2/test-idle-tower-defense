using System.Collections;
using Data;
using UnityEngine;

public class Fortress : MonoBehaviour
{
    [SerializeField] private FortressLevelsData _fortressLevelsData;
    [SerializeField] private EnemyDetector _enemyDetector;
    [SerializeField] private Projectile _projectilePrefab;
    [SerializeField] private EnemyWaveManager _enemyWaveManager;

    public int Damage
    {
        get => _fortressLevelsData.Damages[_currentDamageLevel].Value;
    }
    public float FireRate
    {
        get => _fortressLevelsData.FireRate[_currentFireRateLevel].Value;
    }
    public float FireRange
    {
        get => _fortressLevelsData.FireRange[_currentFireRangeLevel].Value;
    }
    public int UpgradeDamagePrice
    {
        get => _fortressLevelsData.Damages[_currentDamageLevel].UpgradePrice;
    }
    public int UpgradeFireRatePrice
    {
        get => _fortressLevelsData.FireRate[_currentDamageLevel].UpgradePrice;
    }
    public int UpgradeFireRangePrice
    {
        get => _fortressLevelsData.FireRate[_currentDamageLevel].UpgradePrice;
    }
    public bool IsDamageUpgradable
    {
        get => _currentDamageLevel + 1 < _fortressLevelsData.Damages.Count;
    }
    public bool IsFireRateUpgradable
    {
        get => _currentFireRateLevel + 1 < _fortressLevelsData.FireRate.Count;
    }
    public bool IsFireRangeUpgradable
    {
        get => _currentFireRangeLevel + 1 < _fortressLevelsData.FireRange.Count;
    }
    private bool _cannonIsReady = true;

    private int _currentDamageLevel;
    private int _currentFireRateLevel;
    private int _currentFireRangeLevel;

    private void Start()
    {
        SetDefaultOptions();
    }

    public void SetDefaultOptions()
    {
        _currentDamageLevel = _currentFireRateLevel = _currentFireRangeLevel = 0;
        _enemyDetector.SetRadius(_fortressLevelsData.FireRange[_currentFireRangeLevel].Value);
    }

    public bool UpgradeDamage()
    {
        if (_currentDamageLevel + 1 < _fortressLevelsData.Damages.Count)
        {
            _currentDamageLevel++;
        }
        
        return IsDamageUpgradable;
    }

    public bool UpgradeFireRate()
    {
        if (_currentFireRateLevel + 1 < _fortressLevelsData.FireRate.Count)
        {
            _currentFireRateLevel++;
        }

        return IsFireRateUpgradable;
    }

    public bool UpgradeFireRange()
    {
        if (_currentFireRangeLevel + 1 < _fortressLevelsData.FireRange.Count)
        {
            _currentFireRangeLevel++;
            _enemyDetector.SetRadius(_fortressLevelsData.FireRange[_currentFireRangeLevel].Value);
        }

        return IsFireRangeUpgradable;
    }

    private void Update()
    {
        if (_cannonIsReady && _enemyDetector.TryGetClosestEnemy(out Enemy enemy))
        {
            StartCoroutine(Shoot(enemy));
        }
    }

    public void TakeDamage()
    {
        _enemyWaveManager.GameOver();
    }

    private IEnumerator Shoot(Enemy enemy)
    {
        _cannonIsReady = false;
        Projectile projectle = Instantiate(_projectilePrefab, transform.position, Quaternion.identity);
        projectle.Init(enemy.transform.position, _fortressLevelsData.Damages[_currentDamageLevel].Value);
        if (projectle.Damage >= enemy.Health)
        {
            _enemyDetector.RemoveEnemy(enemy);
        }
        yield return new WaitForSeconds(_fortressLevelsData.FireRate[_currentFireRateLevel].Value);
        _cannonIsReady = true;
    }
}