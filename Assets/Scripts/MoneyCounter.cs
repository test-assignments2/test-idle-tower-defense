using UI;
using UnityEngine;

namespace DefaultNamespace
{
    public class MoneyCounter : MonoBehaviour
    {
        public int Money { get; private set; }
        private BottomPanel _bottomPanel;
        
        public void Init(BottomPanel bottomPanel)
        {
            _bottomPanel = bottomPanel;
        }
        
        public void AddMoney(int money)
        {
            Money += money;
            _bottomPanel.SetMoney(Money);
        }

        public bool TrySpendMoney(int money)
        {
            if (Money >= money)
            {
                Money -= money;
                _bottomPanel.SetMoney(Money);
                return true;
            }

            return false;
        }

        public void Reset()
        {
            Money = 0;
            _bottomPanel.SetMoney(Money);
        }
    }
}