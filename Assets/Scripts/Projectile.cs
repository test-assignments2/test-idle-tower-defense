using UnityEngine;

[RequireComponent(
    typeof(Rigidbody2D),
    typeof(Collider2D))]
public class Projectile : MonoBehaviour
{
    [SerializeField] private float _speed;
    public int Damage { get; private set; }

    public void Init(Vector2 target, int damage)
    {
        Damage = damage;
        gameObject.GetComponent<Rigidbody2D>().velocity = target.normalized * _speed;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent(out Enemy enemy))
        {
            enemy.TakeDamage(Damage);
            Destroy(gameObject);
        }
    }
}