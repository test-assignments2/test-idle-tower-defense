using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BottomPanel : MonoBehaviour
    {
        [SerializeField] private Button _damageUpgradeButton;
        [SerializeField] private TextMeshProUGUI _damageUpgradeText;
        [SerializeField] private Button _fireRateUpgradeButton;
        [SerializeField] private TextMeshProUGUI _fireRateUpgradeText;
        [SerializeField] private Button _fireRangeUpgradeButton;
        [SerializeField] private TextMeshProUGUI _fireRangeUpgradeText;
        [Space]
        [SerializeField] private TextMeshProUGUI _moneyText;
        [SerializeField] private Slider _waveSlider;
        [SerializeField] private TextMeshProUGUI _waveText;

        private Fortress _fortress;
        private MoneyCounter _moneyCounter;
        private bool _damageIsMaxLevel;
        private bool _fireRateIsMaxLevel;
        private bool _fireRangeIsMaxLevel;

        public void Init(Fortress fortress, MoneyCounter moneyCounter)
        {
            _fortress = fortress;
            _moneyCounter = moneyCounter;
            _moneyCounter.Init(this);
        }

        public void SetDefaultOptions()
        {
            SetButtonsStatus();
            SetDamageText();
            SetFireRateText();
            SetFireRangeText();
        }

        public void SetMoney(int money)
        {
            _moneyText.text = $"Coins: {money}";
            SetButtonsStatus();
        }

        public void SetWave(int wave, float maxValue)
        {
            _waveText.text = $"Wave: {wave}";
            _waveSlider.maxValue = maxValue;
            _waveSlider.value = maxValue;
        }

        public void SetWaveValue(float value)
        {
            _waveSlider.value = value;
        }
        private void OnEnable()
        {
            _damageUpgradeButton.onClick.AddListener(UpgradeDamage);
            _fireRateUpgradeButton.onClick.AddListener(UpgradeFireRate);
            _fireRangeUpgradeButton.onClick.AddListener(UpgradeFireRange);
        }

        private void OnDisable()
        {
            _damageUpgradeButton.onClick.RemoveListener(UpgradeDamage);
            _fireRateUpgradeButton.onClick.RemoveListener(UpgradeFireRate);
            _fireRangeUpgradeButton.onClick.RemoveListener(UpgradeFireRange);
        }

        private void UpgradeDamage()
        {
            if (_fortress.IsDamageUpgradable && _moneyCounter.TrySpendMoney(_fortress.UpgradeDamagePrice))
            {
                if (!_fortress.UpgradeDamage())
                {
                    _damageIsMaxLevel = true;
                }
                SetDamageText();
            }

            SetButtonsStatus();
        }

        private void UpgradeFireRate()
        {
            if (_fortress.IsFireRateUpgradable && _moneyCounter.TrySpendMoney(_fortress.UpgradeFireRatePrice))
            {
                if (!_fortress.UpgradeFireRate())
                {
                    _fireRateIsMaxLevel = true;
                }
                SetFireRateText();
            }

            SetButtonsStatus();
        }

        private void UpgradeFireRange()
        {
            if (_fortress.IsFireRangeUpgradable && _moneyCounter.TrySpendMoney(_fortress.UpgradeFireRangePrice))
            {
                if (!_fortress.UpgradeFireRange())
                {
                    _fireRangeIsMaxLevel = true;
                }
                SetFireRangeText();
            }

            SetButtonsStatus();
        }

        private void SetDamageText()
        {
            _damageUpgradeText.text = $"Damage: {_fortress.Damage}.\n" +
                                      $"Upgrade for {_fortress.UpgradeDamagePrice} coins.";
            if (_damageIsMaxLevel)
            {
                _damageUpgradeText.text = $"Damage: {_fortress.Damage}.\n" +
                                          $"MAX LEVEL.";
            }
        }

        private void SetFireRateText()
        {
            _fireRateUpgradeText.text =
                $"Fire Rate: {_fortress.FireRate}.\n" +
                $"Upgrade for {_fortress.UpgradeFireRatePrice} coins.";
            if (_fireRateIsMaxLevel)
            {
                _fireRateUpgradeText.text =
                    $"Fire Rate: {_fortress.FireRate}.\n" +
                    $"MAX LEVEL.";
            }
        }

        private void SetFireRangeText()
        {
            _fireRangeUpgradeText.text =
                $"Fire Range: {_fortress.FireRange}.\n" +
                $"Upgrade for {_fortress.UpgradeFireRangePrice} price.";
            if (_fireRangeIsMaxLevel)
            {
                _fireRangeUpgradeText.text =
                    $"Fire Range: {_fortress.FireRange}.\n" +
                    $"MAX LEVEL";
            }
        }

        private void SetButtonsStatus()
        {
            _damageIsMaxLevel = !_fortress.IsDamageUpgradable;
            _fireRateIsMaxLevel = !_fortress.IsFireRateUpgradable;
            _fireRangeIsMaxLevel = !_fortress.IsFireRangeUpgradable;
            
            _damageUpgradeButton.interactable = _moneyCounter.Money >= _fortress.UpgradeDamagePrice && !_damageIsMaxLevel;
            _fireRateUpgradeButton.interactable = _moneyCounter.Money >= _fortress.UpgradeFireRatePrice && !_fireRateIsMaxLevel;
            _fireRangeUpgradeButton.interactable = _moneyCounter.Money >= _fortress.UpgradeFireRangePrice && !_fireRangeIsMaxLevel;
        }
    }
}