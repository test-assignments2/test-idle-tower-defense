namespace UI
{
    public enum GameOverStatus
    {
        Win,
        Lose
    }
}