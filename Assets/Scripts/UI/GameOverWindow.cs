using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal class GameOverWindow : MonoBehaviour
    {
        [SerializeField] private GameObject _winBackgound;
        [SerializeField] private GameObject _loseBackgound;
        [SerializeField] private Button _closeButton;
        [SerializeField] private TextMeshProUGUI _text;

        private RootUI _rootUI;

        public void Init(RootUI rootUI)
        {
            _rootUI = rootUI;
        }

        public void EnableWin()
        {
            _loseBackgound.SetActive(false);
            _winBackgound.SetActive(true);
            _text.text = $"Game\nOver\n\nYou Win!";
        }

        public void EnableLose()
        {
            _winBackgound.SetActive(false);
            _loseBackgound.SetActive(true);
            _text.text = $"Game\nOver\n\nYou Lose!";
        }
        //
        // public void SetText(string text)
        // {
        //     _text.text = text;
        // }

        private void OnEnable()
        {
            _closeButton.onClick.AddListener(_rootUI.ShowStart);
        }

        private void OnDisable()
        {
            _closeButton.onClick.RemoveAllListeners();
        }
    }
}