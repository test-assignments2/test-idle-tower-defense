using DefaultNamespace;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
    public class RootUI : MonoBehaviour
    {
        [SerializeField] private EnemyWaveManager _enemyWaveManager;
        [SerializeField] private Fortress _fortress;
        [SerializeField] private MoneyCounter _moneyCounter;
        [Header("UI")]
        [SerializeField] private GameOverWindow _gameOverWindow;
        [SerializeField] private StartGameWindow _startGameWindow;
        [SerializeField] private BottomPanel _bottomPanel;
        
        public BottomPanel BottomPanel {get => _bottomPanel;}
        
        private void Awake()
        {
            _startGameWindow.Init(_enemyWaveManager);
            _gameOverWindow.Init(this);
            _bottomPanel.Init(_fortress, _moneyCounter);
            ShowStart();
        }

        public void ShowGameOver(GameOverStatus status)
        {
            switch (status)
            {
                case GameOverStatus.Win:
                    _gameOverWindow.EnableWin();
                    break;
                case GameOverStatus.Lose:
                    _gameOverWindow.EnableLose();
                    break;
            }
            _gameOverWindow.gameObject.SetActive(true);
        }

        public void ShowStart()
        {
            _fortress.SetDefaultOptions();
            _gameOverWindow.gameObject.SetActive(false);
            _startGameWindow.gameObject.SetActive(true);
            _bottomPanel.SetDefaultOptions();
        }
    }
}