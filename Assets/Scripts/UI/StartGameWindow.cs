using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal class StartGameWindow : MonoBehaviour
    {
        [SerializeField] private Button _startGameButton;

        private EnemyWaveManager _enemyWaveManager;

        public void Init(EnemyWaveManager enemyWaveManager)
        {
            _enemyWaveManager = enemyWaveManager;
        }

        private void OnEnable()
        {
            _startGameButton.onClick.AddListener(StartGame);
        }

        private void OnDisable()
        {
            _startGameButton.onClick.RemoveListener(StartGame);
        }

        private void StartGame()
        {
            _enemyWaveManager.StartGame();
            gameObject.SetActive(false);
        }
    }
}